﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class VideoPlayer : MonoBehaviour {
	UniversalMediaPlayer mediaPlayer;
	public UnityEvent OnStopped;
	public float playDuration = 15.0f;

	void Start(){
		
		mediaPlayer = GameObject.FindObjectOfType<UniversalMediaPlayer> ();
		if (mediaPlayer) {
//			print (VideoUrlData.VideoUrl);
			mediaPlayer.Path = VideoUrlData.VideoUrl;
			mediaPlayer.Play ();

//			print (mediaPlayer.Length);
			playDuration = VideoUrlData.videoFreeDuration;
				
			if (VideoUrlData.IsLocked) {
				StartCoroutine (CheckForLenght ());

			}
		}
	}

	IEnumerator CheckForLenght(){

		while (true) {
			
			float currentPosition = mediaPlayer.Time / 1000.0f;
			if (currentPosition >= playDuration) {
				mediaPlayer.Pause ();

				if (OnStopped != null) {
					OnStopped.Invoke ();
				}

				StopAllCoroutines ();
			}
			yield return new WaitForEndOfFrame ();
		}
	}
}
