﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class VideoInfo
{
	public List<Video> videos { get; set; }
}

[System.Serializable]
public class Video
{
	public string id { get; set; }
	public string video_name { get; set; }
	public string description { get; set; }
	public string iap_name { get; set; }
	public string price { get; set; }
	public string is_video_free { get; set; }
	public string video_free_duration { get; set; }
	public string video_url { get; set; }
	public string thumbnail_path { get; set; }
}