﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

public class ReticleFiller : MonoBehaviour, IGvrGazeResponder {


	public GameObject reticle;
	public Image fillerReticleImage;
	public MeshRenderer reticleRenderer;

	Coroutine thisCoroutine;
	public float speed = 1f;

	string VideoUrl = string.Empty;

	public UnityEvent OnFilled;

	// Use this for initialization
	void Start () {
		reticle.SetActive (false);


	}
	
	// Update is called once per frame
	void Update () {
	
	}

	#region IGvrGazeResponder implementation

	public void OnGazeEnter ()
	{
		Debug.Log ("Print");		
	}

	public void OnGazeExit ()
	{
		Debug.Log ("Print");		

	}

	public void OnGazeTrigger ()
	{
	}

	public void OnOver(GameObject target){
		if (thisCoroutine == null) {
			thisCoroutine =	StartCoroutine (FillReticle ());
		}

	}

	public void OnOut(){
		StopAllCoroutines ();
		if (reticle != null) {
			reticle.SetActive (false);
		}
		reticleRenderer.enabled = true;
		thisCoroutine = null;
	}


	IEnumerator FillReticle(){
		float timer = 0f;
		reticle.SetActive (true);
		reticleRenderer.enabled = false;
		fillerReticleImage.fillAmount = 0f;
		do {
			fillerReticleImage.fillAmount = Mathf.Lerp(0f,1f,timer);
			timer += Time.deltaTime * speed;
			yield return new WaitForEndOfFrame();
		} while (timer <= 1f);
		fillerReticleImage.fillAmount = 1f;

		reticle.SetActive (false);
		reticleRenderer.enabled = true;

		if (OnFilled != null) {
			OnFilled.Invoke ();
		}
	}

	#endregion

	public void Load3DHallScene(){
		VideoUrlData.VideoUrl = this.VideoUrl;
		SceneManager.LoadScene ("Three D Movie Hall");
	}

	public void LoadStartScreen(){
		SceneManager.LoadScene ("UMenuGallery");
	}
}
