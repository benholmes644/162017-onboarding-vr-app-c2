﻿using UnityEngine;
using System.Collections;

public class Stereo : MonoBehaviour {

	public LayerMask leftCullingMask;
	public LayerMask rightCullingMask;

	// Use this for initialization
	IEnumerator Start () {

		yield return new WaitForSeconds (0.1f);
		for (int i = 0; i < Camera.allCameras.Length; i++) {
			Camera camera = Camera.allCameras [i];

			if (camera.name == Camera.main.name + " Left") {
				camera.cullingMask = leftCullingMask;
			} else if (camera.name == Camera.main.name + " Right") {
				camera.cullingMask = rightCullingMask;
			}
		}
	}

}
