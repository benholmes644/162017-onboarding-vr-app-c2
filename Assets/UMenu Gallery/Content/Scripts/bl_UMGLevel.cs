﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Sdkbox;

public class bl_UMGLevel : MonoBehaviour {

    public string LevelName = "Level";
	public string stringForPurchase = "IAP";
	public string streamingVideoUrl = string.Empty;
	public string levelDescription = string.Empty;

    public Image PreviewImage = null;
    public bool Unlock = true;
    /// <summary>
    /// Level(XP,Kills,Point,etc...) needed for unlock this level
    /// </summary>
    public int LevelNeeded = 0;
    [Space(5)]
	public Color SelectColor = new Color(0.2f, 0.2f, 0.2f, 0.9f);
    [HideInInspector]
    public bool isSelect = false;

    //Private
    private Color DefaultColor;
	private CanvasGroup parentCanvasGroup;
//	private bl_UMGManager umgManager;

    void Start()
    {
		parentCanvasGroup = gameObject.GetComponentInParent<CanvasGroup> ();
//		umgManager = GameObject.FindObjectOfType<bl_UMGManager> ();
    }

    /// <summary>
    /// 
    /// </summary>
	public void Select()
	{
//		Debug.Log ("inside "+LevelName+" select");
		VideoUrlData.currentVideoPurchaseString = stringForPurchase;
		VideoUrlData.VideoUrl = streamingVideoUrl;
		VideoUrlData.IsLocked = !Unlock;

		bl_UMGManager.instance.UpdateButtonsOnVideoState (Unlock);
		bl_UMGManager.instance.setLevelToLoad ();

		bl_UMGManager.instance.DelectAllOther(LevelName);    
		PreviewImage.color = SelectColor;            

		bl_UMGManager.instance.changeDescription (LevelName);
		bl_UMGManager.instance.setCurrentSelectedVideo (this);
    }

	public void Deselect() {
		PreviewImage.color = DefaultColor;
	}

	public void unlockLevel() {
		Unlock = true;
		bl_UMGManager.instance.UpdateButtonsOnVideoState (Unlock);
		VideoUrlData.IsLocked = false;
	}

    /// <summary>
    /// 
    /// </summary>
    /// <param name="Lname"></param>
    /// <param name="preview"></param>
    /// <param name="LNeeded"></param>
	public void GetInfo(string Lname, string lDescription,Sprite preview,bool isLocked, string pString, string vUrl)
    {
        this.LevelName = Lname;
		this.levelDescription = lDescription;
		stringForPurchase = pString;
		streamingVideoUrl = vUrl;
		PreviewImage.sprite = preview;

		Unlock = !isLocked;

		DefaultColor = PreviewImage.color;
    }
}
