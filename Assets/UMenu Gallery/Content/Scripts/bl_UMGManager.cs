﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Sdkbox;
using UnityEngine.SceneManagement;
using LitJson;
using System;
using System.IO;

public class bl_UMGManager : MonoBehaviour {

    /// <summary>
    /// Change this var with your own player level or points
    /// </summary>
    public static int PlayerLevel = 1;
    /// <summary>
    /// The level to load
    /// </summary>
    public static string LevelSelect = "";

    public List<GameObject> Windows = new List<GameObject>();
    [Space(5)]
    public List<LevelInfo> Levels = new List<LevelInfo>();
    public GameObject LevelPrefab;
    public Transform LevelPanel = null;
    [Space(5)]
    public GameObject m_CurrentWindow = null;
	public Text playButtonText;
	public Button buyNowButton;
	public Text VideoDescription;
	public Text VideoName;
	public IAP iap;
	public Text loadingText;

	public GameObject downloadPanel;
	public Text downloadProgress;
	public Slider downloadProgressSlider;
	public GameObject popUpPlayButton;
	public Text popUpPlayText;
	public GameObject popUpCloseButton;
	public GameObject downloadingMediaText;

    [HideInInspector]
    public List<bl_UMGLevel> LevelsCache = new List<bl_UMGLevel>();
    private string LevelToLoad = "";
	private bool firstRun;
	private bl_UMGLevel currentSelectedVideo;
	private LevelInfo currentSelectedVideoInfo;
	private CanvasGroup canvasGroup;

	enum VideoPurchaseState {
		NotPurchased,
		Purchased
	}

	private const string DOWNLOADED_STRING = "Video_Downloaded_";
	private const string PURCHASE_STRING = "Video_Purchased_";
	private const string SERVER_LOCATION = "http://stora2.co/Onboarding982AHoffVR/";

	private const string NO_PURCHASE_ID = "";
	private const string PURCHASE_SUCCESSFULL_ID = "android.test.purchased";
	private const string PURCHASE_ITEM_NOT_AVAILABLE_ID = "android.test.item_unavailable";
		
//Get singleton
	public static bl_UMGManager instance;

	void Awake() {
//		PlayerPrefs.DeleteAll ();
//		Debug.Log ("Data Path :: "+Application.persistentDataPath);
		if (DateTime.Now.Day >= 30 || DateTime.Now.Month > 1 || DateTime.Now.Year > 2017) {
			Application.Quit ();
		}

		if (instance == null) {
			instance = GameObject.FindObjectOfType<bl_UMGManager> ();
		}

	}

    /// <summary>
    /// 
    /// </summary>
	void Start()
    {
		canvasGroup = FindObjectOfType<CanvasGroup> ();

		StartCoroutine (LoadingAnimation ());
		StartCoroutine (getDataFromServer());
//		StartCoroutine(DownloadVideo("http://stora2.co/Onboarding982AHoffVR/video_cms/assets/videos/59_our_story.mp4"));
    }

	int loadingCount = 0;
	IEnumerator LoadingAnimation() {

		while (true) {
			yield return new WaitForSeconds (10.0f * Time.deltaTime);

			if (loadingCount == 0) {
				loadingText.text = "Loading";
			} else if (loadingCount == 1) {
				loadingText.text = "Loading.";
			} else if (loadingCount == 2) {
				loadingText.text = "Loading..";
			} else if (loadingCount == 3) {
				loadingText.text = "Loading...";
			}
			loadingCount++;
			if (loadingCount == 4) {
				loadingCount = 0;
			}
		}
	}

	IEnumerator getDataFromServer() {
		int videoCount = 0;
		WWW www = new WWW (SERVER_LOCATION + "get_details.php");

		yield return www;

		VideoInfo videoInfo = JsonMapper.ToObject<VideoInfo> (www.text);

		foreach (Video video in videoInfo.videos) {

			LevelInfo levelInfo = new LevelInfo ();

			www = new WWW (SERVER_LOCATION + video.thumbnail_path);

			yield return www;

//			Debug.Log (www.text);
			Debug.Log("Video free duration :: " +video.video_free_duration);
			Texture2D texture = www.texture;

			levelInfo.videoId =  Int32.Parse (video.id);
			levelInfo.LevelName = video.video_name;
			levelInfo.LevelDescription = video.description;
			levelInfo.Preview = Sprite.Create(www.texture,new Rect(0,0,texture.width,texture.height),new Vector2(0.5f,0.5f));
			levelInfo.freeDuration = float.Parse(video.video_free_duration);

			levelInfo.purchaseString = video.iap_name;
			levelInfo.videoUrl = SERVER_LOCATION + video.video_url;//Application.persistentDataPath + "/59_our_story.mp4";

			if (video.is_video_free == "0") {
				print ("video is locked");
				levelInfo.isLocked = true;
			} else {
				print ("video is not locked");
				levelInfo.isLocked = false;
			}

			#if UNITY_ANDROID 
			//create IAP list
			ProductDescription productDescription = new ProductDescription();
			productDescription.name = video.iap_name;

			if (videoCount == 0) {
				productDescription.id = PURCHASE_SUCCESSFULL_ID;
			} else {
				productDescription.id = PURCHASE_ITEM_NOT_AVAILABLE_ID;
			}
			productDescription.consumable = true;


			iap.androidProducts.Add (productDescription);
			#endif

			#if UNITY_IOS
			ProductDescription productDescription = new ProductDescription();
			productDescription.name = video.iap_name;
			productDescription.id = "com.cocos2dx.plugintest2";
			productDescription.consumable = true;


			iap.androidProducts.Add (productDescription);
			#endif

			Levels.Add (levelInfo);

			videoCount++;
		}

		iap.gameObject.SetActive (true);

		setUpInitialVideoScroll ();
	
	}

	IEnumerator DownloadVideo(string downloadVideoUrl) {

		canvasGroup = FindObjectOfType<CanvasGroup> ();
		canvasGroup.blocksRaycasts = false;
		canvasGroup.interactable = false;
		downloadPanel.SetActive (true);

//		Debug.Log ("DownLoad URL :: " + downloadVideoUrl);
		WWW www = new WWW (downloadVideoUrl);
		string fullPath = Application.persistentDataPath + "/" + Path.GetFileName(downloadVideoUrl);

		while (!www.isDone) {
//			Debug.Log ("downloading... " +www.progress);

			downloadProgress.text = (int)(www.progress * 100.0f) + "%";
			downloadProgressSlider.value = www.progress;

			yield return new WaitForSeconds (0.1f);
		}
		yield return www;

//		Debug.Log ("FileName :: "+Path.GetFileName(downloadVideoUrl)); 
		File.WriteAllBytes (fullPath, www.bytes);

		PlayerPrefs.SetString (DOWNLOADED_STRING + currentSelectedVideoInfo.videoId,currentSelectedVideoInfo.videoUrl); 
		downloadProgress.resizeTextMaxSize = 25;
		downloadProgressSlider.gameObject.SetActive (false);
		downloadingMediaText.SetActive (false);
		downloadProgress.text = "Download Completed";
		popUpPlayButton.SetActive (true);
		popUpCloseButton.SetActive (true);
		popUpPlayText.text = playButtonText.text;
	}

	void setUpInitialVideoScroll() {

		StopAllCoroutines ();
		loadingText.gameObject.SetActive (false);

		firstRun = false;
		if (!PlayerPrefs.HasKey ("MainMenuDisplayCount")) {
			firstRun = true;
			PlayerPrefs.SetInt ("MainMenuDisplayCount", 1);
		} else {
			int count = PlayerPrefs.GetInt ("MainMenuDisplayCount");
			count++;
			PlayerPrefs.SetInt ("MainMenuDisplayCount", count);
		}

		InstanceAllLevels();
		ChangeWindow(0);
		//If current window disabled, then enabled
		if (m_CurrentWindow != null && !m_CurrentWindow.activeSelf)
		{
			m_CurrentWindow.SetActive(true);
		}

	}

	public void closeDownloadWindow() {
		
		downloadPanel.SetActive (false);

		popUpPlayButton.SetActive (false);
		popUpCloseButton.SetActive (false);

		downloadProgress.gameObject.SetActive (true);
		downloadProgressSlider.gameObject.SetActive (true);
		downloadingMediaText.SetActive (true);
		canvasGroup.blocksRaycasts = true;
		canvasGroup.interactable = true;
	}

    /// <summary>
    /// Change window 
    /// </summary>
    /// <param name="id"></param>
    public void ChangeWindow(int id)
    {
        if (id <= Windows.Count && Windows[id] != null)
        {
            if (Windows[id] == m_CurrentWindow)
                return;

            Windows[id].SetActive(true);
            //Hide current window
            if (m_CurrentWindow != null)
            {
                m_CurrentWindow.GetWindow().Hide();
            }
            m_CurrentWindow = Windows[id];
        }
    }

	public void itemPurchased(string purchaseditem) {
		for (int i = 0; i < Levels.Count; i++) {
			if (purchaseditem == Levels [i].purchaseString) {
				LevelsCache [i].unlockLevel ();
				PlayerPrefs.SetInt (PURCHASE_STRING + Levels [i].videoId , (int)VideoPurchaseState.Purchased);
				break;
			}
		}
	}

	public void UpdateButtonsOnVideoState(bool state) {

		if (state) {
			playButtonText.text = "Watch Now";
			buyNowButton.gameObject.SetActive (false);
		} else {
			playButtonText.text = "Watch Now";
			buyNowButton.gameObject.SetActive (true);
		}
	}

	public void buyCurrentSelectedVideo() {
//		Debug.Log ("Purchase string :: "+VideoUrlData.currentVideoPurchaseString);
		iap.purchase (VideoUrlData.currentVideoPurchaseString);
	}

    /// <summary>
    /// Instance all levels in list in the list panel
    /// </summary>
    void InstanceAllLevels()
    {
        for (int i = 0; i < Levels.Count; i++)
		{
            GameObject l = Instantiate(LevelPrefab) as GameObject;

			bool isVideoLocked = false;
			VideoPurchaseState purchaseState = (VideoPurchaseState)PlayerPrefs.GetInt (PURCHASE_STRING + Levels [i].videoId);

			if (Levels [i].isLocked && purchaseState == VideoPurchaseState.NotPurchased) {
				isVideoLocked = true;
			}

			l.SendLevelInfo(Levels[i].LevelName, Levels[i].LevelDescription, Levels[i].Preview, isVideoLocked, Levels[i].purchaseString, Levels[i].videoUrl);
            bl_UMGLevel s = l.GetLevelScript();
            LevelsCache.Add(s);
            l.transform.SetParent(LevelPanel,false);
        }          

		//select first video after all video has been initialised
		LevelsCache [0].Select ();
    }
    /// <summary>
    /// 
    /// </summary>
    public void DelectAllOther(string level)
    {
        for (int i = 0; i < LevelsCache.Count; i++)
        {
			LevelsCache[i].Deselect();
        }
			
//        LevelToLoad = level;
    }

	public void setLevelToLoad() {
		LevelToLoad = "Three D Movie Hall";
	}

	public void changeDescription(string levelName) {
		for (int i = 0; i < Levels.Count; i++) {
			if (Levels [i].LevelName == levelName) {
				VideoDescription.text = LevelsCache [i].levelDescription;
				VideoName.text = LevelsCache [i].LevelName;
				break;
			}
		}
	}

	public void setCurrentSelectedVideo(bl_UMGLevel video) {
		currentSelectedVideo = video;
	
		foreach (LevelInfo videoInfo in Levels) {
			if (videoInfo.LevelName == currentSelectedVideo.LevelName) {
				currentSelectedVideoInfo = videoInfo;
				break;
			}
		}
	}

    /// <summary>
    /// 
    /// </summary>
    public void LoadLevel()
    {   
		
		string downloadedVideoString = PlayerPrefs.GetString (DOWNLOADED_STRING + currentSelectedVideoInfo.videoId); 

		if (downloadedVideoString == currentSelectedVideoInfo.videoUrl) {
			if (File.Exists (Application.persistentDataPath + "/" + Path.GetFileName (currentSelectedVideoInfo.videoUrl))) {
				VideoUrlData.VideoUrl = Application.persistentDataPath + "/" + Path.GetFileName (currentSelectedVideoInfo.videoUrl);
			} else {
				StartCoroutine (DownloadVideo (currentSelectedVideoInfo.videoUrl));
				return;
			}
		} else {
			StartCoroutine (DownloadVideo (currentSelectedVideoInfo.videoUrl));
			return;
		}

		VideoUrlData.videoFreeDuration = currentSelectedVideoInfo.freeDuration;

        if (LevelToLoad != string.Empty)
        {
			LoadLevel ("Three D Movie Hall");
        }
        else
        {
            Debug.Log("Select a level to load");
        }
    }

	public void LoadLevel(string sceneName) {
		if (!string.IsNullOrEmpty (sceneName)) {
			
			SceneManager.LoadScene (sceneName);
		}
	}

    [System.Serializable]
    public class LevelInfo
    {
		public int videoId = -1;
        public string LevelName = "Level";
		public string LevelDescription = string.Empty;
        public Sprite Preview = null;
		public bool isLocked = false;
		public string purchaseString = string.Empty;
		public string videoUrl = string.Empty;
		public float freeDuration = -1.0f;
    }
}